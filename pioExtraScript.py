# Import("env")
import os
from pathlib import Path
from os.path import join
import subprocess, sys
from FirmwareUtilities import fwHelper
from SCons.Script import (ARGUMENTS, COMMAND_LINE_TARGETS, AlwaysBuild,
                          Builder, Default, DefaultEnvironment)
                          
env = DefaultEnvironment()
pathBootloaderHex = join(os.getcwd(), 'extFirmwares', 'bootloader.hex')
pathAppBackupHex = join(os.getcwd(), 'extFirmwares', '600W_backup.hex')
pathAppHex = join(env["PROJECT_BUILD_DIR"],env["PIOENV"],'firmware'+'.hex')
pathCompleteImage = join(env["PROJECT_BUILD_DIR"],env["PIOENV"],f'CompleteImage{env["PIOENV"]}'+'.hex')
outputJLinkScript = join(os.getcwd(), '90_Tools', 'upload.jlink')
jf = join(os.getcwd(), '90_Tools', 'Flasher', 'JFlash.exe')
jl = join(os.getcwd(), '90_Tools', 'JLink', 'JLink.exe')

def make_complete_image(*args, **kwargs):
    # print(args)
    # for a in args:
    #     print(a)
    # print(kwargs)
    # print(env["PIOENV"])
    # print(env["PROJECT_BUILD_DIR"])
    # print(join(env["PROJECT_BUILD_DIR"],env["PIOENV"],env["PROGNAME"]+'.hex'))
    # print("$BUILD_DIR/${PROGNAME}.hex")

    outputlog = join(os.getcwd(), '90_Tools', 'OutputLog.txt')
    pathPrj = join(os.getcwd(), '90_Tools', 'Flasher', 'segger_mcu_projects', 'XMC1403X0064.jflash')

    if(not os.path.exists(jf)):
        print("Could not find J-Flash at", jf)
        input("Press <Return> to exit...")
        return -1

    p = subprocess.Popen([jf, 
        '-hide',
        f'-jflashlog{outputlog}', 
        f'-openprj{pathPrj}',
        f'-open{pathBootloaderHex}',
        f'-merge{pathAppBackupHex}',
        f'-merge{pathAppHex}',
        f'-saveas{pathCompleteImage}',
        '-exit'], stdout=sys.stdout, shell=True)
    p.communicate()

    if (p.returncode != 0):
        print('-'*30 +"Begin JFlash log"+'-'*30)
        print(open(outputlog,'r').read())
        print('-'*30 +'End JFlash log'+'-'*30)
    else:
        print(f'{env["PIOENV"]} complete image generated: {pathCompleteImage}')

    if os.path.exists(outputlog):
        os.remove(outputlog)

    return p.returncode

def _jlink_cmd_script_generate():
        commands = [
            "h",
            "loadfile %s" % (pathCompleteImage),
            "r",
            "q"
        ]
        with open(outputJLinkScript, "w") as fp:
            fp.write("\n".join(commands))
        return outputJLinkScript

def upload_complete_image(*args, **kwargs):
    cmd = [jl] + env["UPLOADERFLAGS"] + ['-CommanderScript', _jlink_cmd_script_generate()]
    print(cmd)
    p = subprocess.Popen(cmd, stdout=sys.stdout, shell=True)
    p.communicate()
    return p.returncode

def make_firmware_update_file(*args, **kwargs):
    r = fwHelper.make_firmware_update_file(pathAppHex, pathAppBackupHex, output=Path('FwUpdates'), name="FwUpdate_221006")
    print('Firmware update file: %s' % r)
    return 0

# env.AddPostAction("buildprog", make_complete_image)
env.AddPostAction("buildprog", make_firmware_update_file)

env.AddCustomTarget(
    name="buildCompleteImage",
    dependencies=[
        # "$BUILD_DIR/${PROGNAME}.hex"
    ],
    actions=[
        make_complete_image
    ],
    title="Build image",
    description="Make completed image [bootloader + apps]"
)

env.AddCustomTarget(
    name="uploadImage",
    dependencies=None,
    actions=[
        upload_complete_image
    ],
    title="Upload image",
    description="Upload complete image"
)

env.AddCustomTarget(
    name="makeUpdate",
    dependencies=None,
    actions=[
        # "pip install cryptography",
        make_firmware_update_file
    ],
    title="Make update file",
    description="Make update file to update apps via bootloader"
)

env.AddCustomTarget(
    name="uploadApps",
    dependencies=None,
    actions=[
        # "pip install cryptography",
        make_firmware_update_file
    ],
    title="Upload Apps",
    description="Make update file to update apps via bootloader"
)
