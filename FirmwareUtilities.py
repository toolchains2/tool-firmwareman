from base64 import b64encode, b64decode
import pathlib
from binascii import hexlify
import os
import sys
import hashlib
import shutil
import string
from re import X
from pathlib import Path
from struct import pack
from collections import namedtuple
# from xml.dom.minidom import Element
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import Element
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives import padding

# public static string Decrypt(string cipherText)
# {
#     try
#     {
#         string EncryptionKey = "abc123";
#         cipherText = cipherText.Replace(" ", "+");
#         byte[] cipherBytes = Convert.FromBase64String(cipherText);
#         using (Aes encryptor = Aes.Create())
#         {
#             Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
#             encryptor.Key = pdb.GetBytes(32);
#             encryptor.IV = pdb.GetBytes(16);
#             using (MemoryStream ms = new MemoryStream())
#             {
#                 using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
#                 {
#                     cs.Write(cipherBytes, 0, cipherBytes.Length);
#                     cs.Close();
#                 }
#                 cipherText = Encoding.Unicode.GetString(ms.ToArray());
#             }
#         }
#         return cipherText;
#     }
#     catch
#     {
#         return " ";
#     }

# }


class fwIdentification(object):
    @staticmethod
    def IdentificationGen(manifest: Element):
        xmlStr = ET.tostring(manifest).decode("utf-8").replace("\n", "\r\n")
        xmlBuff = bytes(xmlStr, "utf-16le")
        # print(bytes(xmlStr, "utf-8"))
        # print(hexlify(xmlBuff, ','))
        xmlCipher = fwIdentification.Encypt(xmlBuff)
        # print(hexlify(xmlCipher, ','))
        ct = b64encode(xmlCipher).decode("utf-8")
        # print(ct)
        # ET.dump(root)
        return ct

    @staticmethod
    def Encypt(plainData: bytes):
        __kdf = PBKDF2HMAC(
            algorithm=hashes.SHA1(),
            length=48,
            salt=bytes([0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65,
                       0x64, 0x76, 0x65, 0x64, 0x65, 0x76]),
            iterations=1000,
        )
        total = __kdf.derive(bytes(fwIdentification.__pwd, "utf-8"))
        key = total[0:32]
        iv = total[32:(32+16)]
        # print(hexlify(key, ',').upper())
        # print(hexlify(iv, ',').upper())
        # testdata = "<Configuration>\r\n  <file1>9fc57e637502233fff07579d53c98fc9</file1>\r\n  <file2>c27cce4a2680379100d838426cfd6a3f</file2>\r\n  <fwType>5000</fwType>\r\n  <fwVer>221005</fwVer>\r\n</Configuration>"
        # testdata = bytes([0x3c,0x00,0x43,0x00,0x6f,0x00,0x6e,0x00,0x66,0x00,0x69,0x00,0x67,0x00,0x75,0x00,0x72,0x00,0x61,0x00,0x74,0x00,0x69,0x00,0x6f,0x00,0x6e,0x00,0x3e,0x00,0x0d,0x00,0x0a,0x00,0x20,0x00,0x20,0x00,0x3c,0x00,0x66,0x00,0x69,0x00,0x6c,0x00,0x65,0x00,0x31,0x00,0x3e,0x00,0x39,0x00,0x66,0x00,0x63,0x00,0x35,0x00,0x37,0x00,0x65,0x00,0x36,0x00,0x33,0x00,0x37,0x00,0x35,0x00,0x30,0x00,0x32,0x00,0x32,0x00,0x33,0x00,0x33,0x00,0x66,0x00,0x66,0x00,0x66,0x00,0x30,0x00,0x37,0x00,0x35,0x00,0x37,0x00,0x39,0x00,0x64,0x00,0x35,0x00,0x33,0x00,0x63,0x00,0x39,0x00,0x38,0x00,0x66,0x00,0x63,0x00,0x39,0x00,0x3c,0x00,0x2f,0x00,0x66,0x00,0x69,0x00,0x6c,0x00,0x65,0x00,0x31,0x00,0x3e,0x00,0x0d,0x00,0x0a,0x00,0x20,0x00,0x20,0x00,0x3c,0x00,0x66,0x00,0x69,0x00,0x6c,0x00,0x65,0x00,0x32,0x00,0x3e,0x00,0x63,0x00,0x32,0x00,0x37,0x00,0x63,0x00,0x63,0x00,0x65,0x00,0x34,0x00,0x61,0x00,0x32,0x00,0x36,0x00,0x38,0x00,0x30,0x00,0x33,0x00,0x37,0x00,0x39,0x00,0x31,0x00,0x30,0x00,0x30,0x00,0x64,0x00,0x38,0x00,0x33,0x00,0x38,0x00,0x34,0x00,0x32,0x00,0x36,0x00,0x63,0x00,0x66,0x00,0x64,0x00,0x36,0x00,0x61,0x00,0x33,0x00,0x66,0x00,0x3c,0x00,0x2f,0x00,0x66,0x00,0x69,0x00,0x6c,0x00,0x65,0x00,0x32,0x00,0x3e,0x00,0x0d,0x00,0x0a,0x00,0x20,0x00,0x20,0x00,0x3c,0x00,0x66,0x00,0x77,0x00,0x54,0x00,0x79,0x00,0x70,0x00,0x65,0x00,0x3e,0x00,0x35,0x00,0x30,0x00,0x30,0x00,0x30,0x00,0x3c,0x00,0x2f,0x00,0x66,0x00,0x77,0x00,0x54,0x00,0x79,0x00,0x70,0x00,0x65,0x00,0x3e,0x00,0x0d,0x00,0x0a,0x00,0x20,0x00,0x20,0x00,0x3c,0x00,0x66,0x00,0x77,0x00,0x56,0x00,0x65,0x00,0x72,0x00,0x3e,0x00,0x32,0x00,0x32,0x00,0x31,0x00,0x30,0x00,0x30,0x00,0x35,0x00,0x3c,0x00,0x2f,0x00,0x66,0x00,0x77,0x00,0x56,0x00,0x65,0x00,0x72,0x00,0x3e,0x00,0x0d,0x00,0x0a,0x00,0x3c,0x00,0x2f,0x00,0x43,0x00,0x6f,0x00,0x6e,0x00,0x66,0x00,0x69,0x00,0x67,0x00,0x75,0x00,0x72,0x00,0x61,0x00,0x74,0x00,0x69,0x00,0x6f,0x00,0x6e,0x00,0x3e,0x00])
        # print("PT: %s" % hexlify(testdata, ','))
        cipher = Cipher(algorithms.AES(key), modes.CBC(iv))
        encryptor = cipher.encryptor()
        # ct = encryptor.update(plainData)
        # print("CT: %s" %  hexlify(ct, ','))
        # fwHelper.debug_print_buffer(key)

        # hashes.SHA1
        # print(len(plainData))

        padder = padding.PKCS7(128).padder()
        a = bytearray(plainData)
        # m =  len(a) % 128
        # if m != 0:
        #     p = bytearray(pack('B', 128 - m))*(128 - m)
        #     a.extend(p)
        a = padder.update(a) + padder.finalize()
        return encryptor.update(a) + encryptor.finalize()

    @staticmethod
    def Decrypt(cipherData):
        key = "abc123"

    __pwd = "abc123"


class fwHelper(object):
    @staticmethod
    def make_complete_image(app: Path, bootloader: Path, backup: Path, output: Path = Path('.'), name: str = 'CompleteImage'):
        return 0

    @staticmethod
    def make_firmware_update_file(app: Path, backup: Path, output: Path = Path('.'), name: str = 'FwUpdate'):
        if output.exists() and output.is_dir():
            outputDirPath = output.joinpath(name)
        else:
            os.mkdir(output)
            outputDirPath = output.joinpath(name)

        aAppbuff = bytearray(fwHelper.ihex_file_to_bytes(app))
        bAppbuff = bytearray(fwHelper.ihex_file_to_bytes(backup))

        outputFWPaths = [outputDirPath.joinpath('1.bin_enc'),
                         outputDirPath.joinpath('2.bin_enc'),
                         outputDirPath.joinpath('manifest.xml'),
                         outputDirPath.joinpath('version.ini'), ]

        if outputDirPath.exists() and outputDirPath.is_dir():
            shutil.rmtree(outputDirPath)
        os.mkdir(outputDirPath)

        with open(outputFWPaths[0], "wb") as bf:
            for i, b in enumerate(aAppbuff):
                aAppbuff[i] = (b ^ (i & 0xff))
            bf.write(aAppbuff)
        md51 = str(hashlib.md5(open(outputFWPaths[0], 'rb').read()).hexdigest())

        with open(outputFWPaths[1], "wb") as bf:
            for i, b in enumerate(bAppbuff):
                bAppbuff[i] = (b ^ (i & 0xff))
            bf.write(bAppbuff)
        md52 = str(hashlib.md5(open(outputFWPaths[1], 'rb').read()).hexdigest())

        root = Element('Configuration')
        root.extend([
            Element('file1'),
            Element('file2'),
            Element('fwType'),
            Element('fwVer'),
        ])

        root.find('file1').text = md51
        root.find('file2').text = md52
        root.find('fwType').text = '5000'
        root.find('fwVer').text = '221004'

        ET.indent(root)
        ET.ElementTree(root).write(outputFWPaths[2])        

        with open(outputFWPaths[3], "w") as tf:
            tf.write(fwIdentification.IdentificationGen(root))

        r = shutil.make_archive(outputDirPath, 'zip', outputDirPath)
        shutil.rmtree(outputDirPath)

        return r

    @staticmethod
    def ihex_file_to_bytes(file):
        start_addr = 0
        # data = bytes()
        str_list = []
        strData = ''

        sr = open(file, 'r')
        # print(sr.readline())
        line = sr.readline()
        # print(line)
        line1 = line[7:9]
        # print(line1)
        match int(line[7:9]):
            case 2:
                line1 = line[9:(9+4)]
                start_addr = int(line1, 16) * 16
            case 4:
                line1 = line[9:(9+4)]
                start_addr = int(line1, 16) << 16
        # print(int(line1))

        # print(hex(start_addr))

        rest = sr.readlines()
        start_addr += int(rest[0][3:(3+4)], 16)
        # print(hex(start_addr))
        # print(rest)
        for il in rest:
            l = il.rstrip('\n')
            Length = int(l[1:(1+2)], 16)
            Addr = int(l[3:(3+4)], 16)
            recordType = int(l[7:(7+2)], 16)

            if (recordType == 0):
                str_list.append(l[9:(9+Length*2)])

        strData = ''.join(str_list)
        data = bytes.fromhex(strData)
        return data

    @staticmethod
    def debug_print_buffer(buf):
        for d in buf:
            if d == buf[-1]:
                # print(hexlify(pack('B', d)))
                print(hex(d))
                # print(pack('B', d))
            else:
                # print(hexlify(pack('B', d)), end =",")
                print(hex(d), end=",")
                # print(pack('B', d), end =",")
        # print()


# from pathlib import *

# appA = 'C:\\Users\\edric_le\\Documents\\PlatformIO\\Projects\\Charger\\.pio\\build\\Debug\\600W.hex'
# appB = pathlib.PurePath(
#     "C:/Users/edric_le/Documents/PlatformIO/Projects/Charger/extFirmwares/600W_backup.hex")
# # # fwHelper.ihex_file_to_bytes(tst)
# fwHelper.make_firmware_update_file(appA, appB)
